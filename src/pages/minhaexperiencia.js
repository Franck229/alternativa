
import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Container, Col, Row} from 'reactstrap';


class Minhaexperiencia extends Component {
    render(){

        return (
            <div >
            
                
   <header className="App-header headerbackgoung fundoVazado03">
     <nav className= "margemTzero navtransparente">
      <div class="nav-wrapper ">
       {/*      <a href="#" class="brand-logo">Logo</a>*/}
      <ul id="nav-mobile" class="right hide-on-med-and-down">

        <li>
        <Link to="/Minhahistoria">
         
         <a href="">Minha história</a>
        </Link> 

        </li>
        <li>
        <Link to="/Meusinteresses">
        <a href="">Meus Interesses</a>
        </Link> 
      </li>
        <li><Link to="/Minhaexperiencia">
         <a href="">Minha experiência</a>
        </Link> 
       </li>
      </ul>
   {/*   <a href="#" data-target="slide-out" class="sidenav-trigger show-on-large"> <i class="large material-icons"></i>   </a>*/}

    </div>
     </nav>

    
    </header>

       <div className="espacamentoTopo">
  

    	<Container >
		<Row>
			<Col md="12" lg="12">
			<h4>  Minha experiência </h4>
			<p className="textoUnidade"> Tenho experiência com o desenvolvimento Mobile (Flutter principalmente), Web (react-native) e no E-commerce. 
			Para saber mais, acesse meu  <a href="https://www.linkedin.com/in/kuassi-dodji-franck-kumako-566bbb110">Linkedin</a>
			</p>
			</Col>
			
		</Row>
	   </Container>



     </div>

     <footer class="page-footer navtransparente footerbackground">
    <div class="container">
      <div class="row">
        <div class="col l6 s12 ">
          <h5 class="white-text">Endereço</h5>
          <p class="grey-text text-lighten-4"> R. Eng. Agronômico Andrei Cristian Ferreira, s/n - Trindade, Florianópolis - SC, 88040-900, Brasil</p>
        </div>
        <div class="col l4 offset-l2 s12">
          <h5 class="white-text">Links</h5>
          <ul>
           

        <li>
        <Link to="/">
         
         <a class="grey-text text-lighten-3" href="">Home</a>
        </Link> 

        </li>

           <li>
        <Link to="/Minhahistoria">
         
         <a href="">Minha história</a>
        </Link> 

        </li>
        <li>
        <Link to="/Meusinteresses">
        <a class="grey-text text-lighten-3" href="">Meus Interesses</a>
        </Link> 
      </li>
        
        
          </ul>
        </div>

      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        © 2014 Copyright Text
       {/* <a class="grey-text text-lighten-4 right" href="#!">More Links</a>*/}
      </div>
    </div>
  </footer>

            </div>
          );
    }
}



export default Minhaexperiencia;
