import React from 'react';
import { Link } from "react-router-dom";
import beninBrasil from "../images/beninBrasil.png";
import benin from "../images/benin.png";
import benin2 from "../images/NonRetour.jpeg";
import videoPortugues from "../video/groupodo Kuassi.mp4";
import { Container, Col, Row} from 'reactstrap';
const Minhahistoria = () => (
    <div>
       
   <header className="App-header headerbackgoung fundoVazado02">
     <nav className= "margemTzero navtransparente">
      <div class="nav-wrapper ">
       {/*      <a href="#" class="brand-logo">Logo</a>*/}
      <ul id="nav-mobile" class="right hide-on-med-and-down">

        <li>
        <Link to="/Minhahistoria">
         
         <a href="">Minha história</a>
        </Link> 

        </li>
        <li>
        <Link to="/Meusinteresses">
        <a href="">Meus Interesses</a>
        </Link> 
      </li>
        <li><Link to="/Minhaexperiencia">
         <a href="">Minha experiência</a>
        </Link> 
       </li>
      </ul>
   {/*   <a href="#" data-target="slide-out" class="sidenav-trigger show-on-large"> <i class="large material-icons"></i>   </a>*/}

    </div>
     </nav>

    
    </header>


 
     <div className="espacamentoTopo">
         
     	<Container class="flow-text">
     	<h4>  Minha história </h4>
		<Row>
			<Col md="6" lg="6" className="" >
			<p className="" > Meu nome é Kuassi Dodji Franck KUMAKO. Em 1993, Nasci no interior da Républica do Benim em uma cidade chamada de Tori-Bossito de paí professor de inglês e uma mãe comerciante. Alguns anos depois, mudarmos para Cotonou ( capital do Benim ) onde eu cresci. Aos 18 anos, ingressei a Universidade Cátólica da África do Oeste no curso de Engenharia Elétrica e ciências da computação.
			 Em 2015, Além de passar no Examen nacional de Tecnico Superior em Informática Industrial, fui selecionado pelo MEC para ir estudar no Brasil ( através de um programa de estudo chamado de PEC-G) . Me formei então como Tecnico superior no benim e viagei em seguida para a cidade de Porto Alegre onde eu estudei o Português na faculdade den Letras da UFRGS. 
			 Depois de passar no Exame de Proficiência em Língua Portuguesa para Estrangeiros (CELPE-Bras), me mudei para Araranguá (cidade de Santa Catarina) onde eu iniciei o Curso de Tecnologias da Informação e Comunicaçao na USFC. Em 2017, me tranferi para o Curso de ciencias da computaçao da mesma universidade em Florianópolis eu vivo, estudo e trabalho até o momento.
			 A segui, um video realizado nas primeiras semenas do curso de Português na UFRGS. Quardei como lembrança de como tudo iniciou no brasil!
			</p>
			</Col>
			<Col md="6" lg="6">
              {/*<h4> Video realizado nas primeiras semenas do curso de Português na UFRGS. Quardei como lembrança de como tudo iniciou no brasil!</h4>*/}
            <video class="responsive-video" controls>
             <source src={videoPortugues} type="video/mp4"/>
            </video>
			{/*<p className="textoUnidade">Neste momento, articular com os setores diretamente relacionados aos problemas detectados é a melhor forma de solucioná-los de maneira efetiva. Você deve usar essas informações sobre as condições de vida e saúde da população para o desenvolvimento e acompanhamento dessas ações intersetoriais. Assim, você poderá contribuir para a efetivação dessas modificações no território e acompanhar as melhorias na saúde da população.
			</p>*/}
			</Col>
		</Row>
	</Container>
         


     </div>

    <div>
   

    </div>

     <footer class="page-footer navtransparente footerbackground">
    <div class="container">
      <div class="row">
        <div class="col l6 s12 ">
          <h5 class="white-text">Endereço</h5>
          <p class="grey-text text-lighten-4"> R. Eng. Agronômico Andrei Cristian Ferreira, s/n - Trindade, Florianópolis - SC, 88040-900, Brasil</p>
        </div>
        <div class="col l4 offset-l2 s12">
          <h5 class="white-text">Links</h5>
          <ul>
           

        <li>
        <Link to="/">
         
         <a class="grey-text text-lighten-3" href="">Home</a>
        </Link> 

        </li>
        <li>
        <Link to="/Meusinteresses">
        <a class="grey-text text-lighten-3" href="">Meus Interesses</a>
        </Link> 
      </li>
        <li><Link to="/Minhaexperiencia">
         <a class="grey-text text-lighten-3" href="">Minha experiência</a>
        </Link> 
       </li>
        
          </ul>
        </div>

      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        © 2014 Copyright Text
       {/* <a class="grey-text text-lighten-4 right" href="#!">More Links</a>*/}
      </div>
    </div>
  </footer>

    </div>
);

export default Minhahistoria;