import logo from './logo.svg';
import './App.css';
import React from 'react';
import 'materialize-css/dist/css/materialize.min.css';
import meusInteresses from './pages/meusInteresses';
import minhaExperiencia from './pages/minhaexperiencia';
import minhahistoria from './pages/minhahistoria';
import { Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Col, Row} from 'reactstrap';
import FranckProfile from "./images/FranckProfile.jpg";
import FranckE from "./images/FranckE.jpg";
import cap from "./images/cap.png";
import TagManager from 'react-gtm-module'

const tagManagerArgs = {
  gtmId: 'GTM-MSPTKW7'
}

TagManager.initialize(tagManagerArgs)

function App() {
  return (
    <div className="App">
   <header className="App-header headerbackgoung fundoVazado01">


     <nav className= "margemTzero navtransparente">
      <div class="nav-wrapper ">
      
      <ul id="nav-mobile" class="right hide-on-med-and-down">

        <li>
        <Link to="/Minhahistoria">
         
         <a href="">Minha história</a>
        </Link> 

        </li>
        <li>
        <Link to="/Meusinteresses">
        <a href="">Meus Interesses</a>
        </Link> 
      </li>
        <li><Link to="/Minhaexperiencia">
         <a href="">Minha experiência</a>
        </Link> 
       </li>
      </ul>
   

    </div>
     </nav>

    </header>



    <Container>

    <Row>
      
     
      <Col md="6" lg="6">


      <div class="row espacamentoTopo">
      <div class="col s12 m6 offset-m3">
        <div class="card">
          <div class="card">
            <div class="card-action grey lighten-5">
              <div class="row valign-wrapper">
                <div class="col s2">
                  <img src="https://avatars0.githubusercontent.com/u/24758932?s=400&u=077a5bd2fec649981ee05e36781179aff7de7578&v=4" alt=""
                    class="circle responsive-img alt= "/>

                 {/* <!-- notice the "circle" class -->*/}

                </div>
                <div class="col s10">
                  <span class="black-text">
                    hello guys!
                  </span>
                </div>
              </div>
            </div>
            <div class="card-image empty-state">
          <img src={FranckE} className="" />
            
            </div>
            <div class="card-content">
              <p></p>
            </div>
            
          </div>
        </div>
      </div>
     </div>

      </Col>


      <Col md="6" lg="6">
      <div class="col s12 m7 espacamentoTopo" >
   
    <div class="card horizontal">
      <div class="card-image">
      <img src={FranckProfile} className="" />
      
      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>Franck KUMAKO,
           Técnico Superior em Informática Industrial e estudante de ciências da computação.</p>
        </div>
        <div class="card-action">
          <a href="https://www.linkedin.com/in/kuassi-dodji-franck-kumako-566bbb110">Linkedin</a>
        </div>
      </div>
    </div>
    </div>
            
     
      </Col>
    </Row>

    </Container>

    


   


   
     <footer class="page-footer navtransparente footerbackground">
    <div class="container">
      <div class="row">
        <div class="col l6 s12 ">
          <h5 class="white-text">Endereço</h5>
          <p class="grey-text text-lighten-4"> R. Eng. Agronômico Andrei Cristian Ferreira, s/n - Trindade, Florianópolis - SC, 88040-900, Brasil</p>
        </div>
        <div class="col l4 offset-l2 s12">
          <h5 class="white-text">Links</h5>
          <ul>
           

             <li>
        <Link to="/Minhahistoria">
         
         <a class="grey-text text-lighten-3" href="">Minha história</a>
        </Link> 

        </li>
        <li>
        <Link to="/Meusinteresses">
        <a class="grey-text text-lighten-3" href="">Meus Interesses</a>
        </Link> 
      </li>
        <li><Link to="/Minhaexperiencia">
         <a class="grey-text text-lighten-3" href="">Minha experiência</a>
        </Link> 
       </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        © 2014 Copyright Text
      
      </div>
    </div>
  </footer>



    </div>
  );
}

export default App;
