import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'materialize-css/dist/css/materialize.min.css';
import Meusinteresses from './pages/meusInteresses.js'
import Minhahistoria from './pages/minhahistoria.js'
import Minhaexperiencia from './pages/minhaexperiencia.js'
import 'bootstrap/dist/css/bootstrap.min.css';

import { Switch, Route, HashRouter } from "react-router-dom";
ReactDOM.render(

 <HashRouter>
		<Switch>
			<Route path="/" exact={true} component={App} />
			<Route path="/minhaexperiencia" component={Minhaexperiencia} />
			<Route path="/meusInteresses" component={Meusinteresses} />
			<Route path="/minhahistoria" component={Minhahistoria} />
			
			
		</Switch>
	</HashRouter>,

  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
